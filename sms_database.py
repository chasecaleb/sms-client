#!/usr/bin/env python
import sqlite3
import collections
import re


# TODO: move this to separate file, implement across project
class _Database:
    """ Base class that provides an interface/wrapper to sqlite3 database.

    Not particularly useful on its own, intended to be subclassed.
    """

    def __init__(self, location):
        self._conn = sqlite3.connect(location)
        self._conn.row_factory = self._namedtuple_factory

    def _execute(self, *command):
        """ Internal helper function for sqlite3 calls. """
        cur = self._conn.cursor()
        cur.execute(*command)
        self._conn.commit()
        return cur

    def _namedtuple_factory(self, cursor, row):
        fields = [col[0] for col in cursor.description]
        try:
            Row = collections.namedtuple("Row", fields)
        # Column names like "max(msg_id)" tend to break things
        except ValueError:
            return row
        return Row(*row)

    def _dict_to_tuple(self, row):
        column_names = [col[0] for col in
                        self._execute(self._select).description]
        return [row[i] for i in column_names]


class _TableAsList(_Database):
    """ Database table interface; mostly works like a list of namedtuples.

    Elements correspond to rows in database table.
    """

    def __init__(self, location, table):
        super().__init__(location)
        self._table = table
        self._select = "SELECT * FROM " + table

        statement = "PRAGMA table_info('" + table + "')"
        result = self._execute(statement).fetchone()
        self._key_name = result.name
        self._key_type = int if result.type == "INTEGER" else str

    def append(self, item):
        """ Accepts dict and list/tuple/namedtuple-like items.

        WARNING: Will overwrite old row if primary key (item[0]) not unique.
        """
        if isinstance(item, dict):
            item = self._dict_to_tuple(item)
        elif not isinstance(item, (list, tuple)):
            raise TypeError
        placeholders = "?, " * (len(item) - 1) + "?"
        statement = "REPLACE INTO", self._table, "VALUES(", placeholders, ")"
        self._execute(' '.join(statement), item)

    def __repr__(self):
        return repr(list(self))

    def __len__(self):
        statement = "SELECT Count(*) FROM " + self._table
        return self._execute(statement).fetchone()[0]

    def __iter__(self):
        return iter(self._execute(self._select).fetchall())

    def remove(self, primary_key):
        """ Delete item from list by primary key.
        Args:
            primary_key: Primary key value used by database, *not* list index.
            Check self._key_name for name of primary_key if unsure.
        Return:
            None if deleted, raise ValueError if primary_key not found.
        """
        # find() will raise ValueError if not found.
        if self.find(primary_key) is not None:
            statement = ("DELETE FROM", self._table, "WHERE", self._key_name, 
                         "= ?")
            self._execute(' '.join(statement), (primary_key,))

    def find(self, primary_key):
        """ Search list by database's primary key column.

        Args:
            primary_key: Value of primary column used by database.
        Return:
            Index value (integer) if found, raise IndexError if not.
        """
        for i, item in enumerate(self):
            if getattr(item, self._key_name) == primary_key:
                return i
        raise ValueError

    def __delitem__(self, index):
        key_value = getattr(self[index], self._key_name)
        self.remove(key_value)

    def __getitem__(self, key):
        def validate_key(k):
            if self._key_type == int and not isinstance(k, int):
                raise TypeError
            elif isinstance(k, int) and k >= len(self):
                raise IndexError

        def fit_key(k):
            if k < 0:
                return len(self) + k
            elif k > len(self):
                return len(self)
            return k

        if isinstance(key, slice):
            start = fit_key(key.start) if key.start is not None else 0
            stop = fit_key(key.stop) if key.stop is not None else len(self)
            return [self[i] for i in range(start, stop)]
        else:
            validate_key(key)
            # Convert negative key to equivalent positive value
            key = len(self) + key if key < 0 else key
            statement = self._select, "LIMIT 1 OFFSET ?"
            result = self._execute(' '.join(statement), (key,)).fetchone()
            if result:
                return result
            raise IndexError if self._key_type == int else KeyError


class Messages(_TableAsList):

    def __init__(self, location):
        super().__init__(location, "sms")
        # ...Easiest way to correlate contact info with threads.
        self._contacts = Contacts(location)

    @property
    def threads(self):
        """ Read-only list containing dict for each thread.

        Element: dict of name and most recent message in each thread.
        """
        def _get_name(address):
            try:
                return self._contacts.lookup_address(address).name
            except AttributeError:
                return address

        def most_recent_message(thread_id):
            statement = ("SELECT * FROM sms WHERE msg_id =",
                         "(SELECT max(msg_id) FROM sms WHERE thread_id = ?)")
            return self._execute(' '.join(statement), (thread_id,)).fetchone()

        cur = self._execute("SELECT DISTINCT thread_id FROM sms")
        thread_ids = [i.thread_id for i in cur]
        result = []
        for i in thread_ids:
            msg = most_recent_message(i)
            result.append({
                'name': _get_name(msg.address),
                'message': msg})
        return result
        # return [most_recent_message(i) for i in thread_ids]

    # @property
    # def threads_by_name(self):
        # """ Read-only list containing contact name of each thread.

        # Falls back to address string if contact is not found.
        # """

        # return [_get_name(thread.address) for thread in self.threads]

    def get_thread_messages(self, thread_id):
        """ Return all messages in thread_id. """
        statement = "SELECT * FROM sms WHERE thread_id = ?"
        return self._execute(statement, (thread_id,)).fetchall()


# TODO: add column to contacts to correlate contacts <-> msg threads.
class Contacts(_TableAsList):

    def __init__(self, location):
        super().__init__(location, "contacts")

    def lookup_address(self, address):
        """ Match address to contact.

        Address:
            Phone number to search for. Will be stripped internally.
        Return:
            Contact row if found, None if not.
        """
        def _strip_phone(address):
            # Strip leading 1 (country code) out of number.
            for i, char in enumerate(address):
                if char.isdigit():
                    if char == '1':
                        offset = i + 1
                        address = address[offset:]
                    break
            # Match groups of 3, 3, and 4 digits, each group separated by any 
            # non-digit character(s). 
            pattern = re.compile(r'(\d{3})\D*(\d{3})\D*(\d{4})')
            try:
                result = pattern.search(address).groups()
                return ''.join(result)
            except AttributeError:
                return None

        stripped = _strip_phone(address)
        for contact in self:
            if stripped == _strip_phone(contact.address):
                return contact
        else:
            return False
