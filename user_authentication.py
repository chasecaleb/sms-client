#!/usr/bin/env python
import sqlite3
import bcrypt
import hashlib
import uuid
import time


class UserAuthentication:
    """ IMPORTANT: make sure to authorize user before doing anything else. """

    def __init__(self, location="users.db"):
        self.location = location

    def get_sms_database(self, username):
        """
        Return:
            Matching sms database filename, False if not found.
        """
        cur = self._execute(
            "SELECT sms_database FROM users WHERE username = ?", (username,))
        result = cur.fetchone()
        return result[0] if result else False

    def get_google_cloud_id(self, username):
        """
        Return:
            google_cloud_id string matching passed username if found, None if
            not.
        """
        cur = self._execute(
            "SELECT google_cloud_id FROM users WHERE username = ?",
            (username,))
        result = cur.fetchone()
        return result[0] if result else None

    def create_account(self, username, password, google_cloud_id):
        """ Insert new account into database.

        Args:
            username: self-explanatory
            password: plain-text (will be hashed)
            google_cloud_id: key for Google Cloud Messaging on user's phone.
        Return:
            True if successful.
            False if account exists (sqlite3.IntegrityError caught).
        """
        database_filename = hashlib.sha256(username.encode()).hexdigest()
        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
        try:
            self._execute("INSERT INTO users VALUES(?, ?, ?, ?)",
                          (username, hashed_password, database_filename,
                           google_cloud_id))
        except sqlite3.IntegrityError:
            return False
        return True

    def authenticate(self, username, password=None, session_id=None):
        """ Compare username and either password or session_id to database.

        If both password and session_id passed, ignores session_id.

        Return:
            True/False if authenticated/not.
        """
        if password:
            statement = "SELECT password FROM users WHERE username = ?"
            cur = self._execute(statement, (username,))
            try:
                return bcrypt.checkpw(password, cur.fetchone()[0])
            except TypeError:
                return False
        elif session_id:
            statement = ("SELECT session_id, expiration FROM sessions WHERE" +
                         " username = ?")
            cur = self._execute(statement, (username,))
            result = cur.fetchone()
            # Make sure that strings and ints aren't compared to each other.
            valid_session = str(session_id) == str(result[0])
            not_expired = time.time() < int(result[1])
            return valid_session and not_expired

    def create_session_id(self, username):
        """ Create, store, and return session_id.

        Session ID's expire after one week, and only one is stored per user.
        """
        session_id = str(uuid.uuid4())
        # 86,400 seconds in a day.
        expiration = int(time.time()) + (7 * 86400)
        statement = "REPLACE INTO sessions VALUES(?, ?, ?)"
        self._execute(statement, (username, session_id, expiration))
        return session_id

    def change_password(self, username, new_password):
        """ Change username's password to new_password.

        IMPORTANT: make sure to call authenticate() to verify user first!

        Return:
            True if successful, False if username not found.
        """
        select = "SELECT password FROM users WHERE username = ?"
        cur = self._execute(select, (username,))
        if cur.fetchone():
            password_hash = bcrypt.hashpw(new_password, bcrypt.gensalt())
            update = "UPDATE users SET password = ? WHERE username = ?"
            self._execute(update, (password_hash, username))
            return True
        return False

    def change_google_cloud_id(self, username, new_key):
        """ Replace current key with passed new_key. 

        Make sure to authenticate user before calling.

        Return:
            True if successful, False if username invalid.
        """
        select = "SELECT username FROM users WHERE username = ?"
        cur = self._execute(select, (username,))
        if cur.fetchone() is not None:
            update = "UPDATE users SET google_cloud_id = ? WHERE username = ?"
            self._execute(update, (new_key, username))
            return True
        return False

    def delete_account(self, username):
        """ Delete account row from table. """
        select = "SELECT username FROM users WHERE username = ?"
        cur = self._execute(select, (username,))
        if cur.fetchone() is not None:
            delete = "DELETE FROM users WHERE username = ?"
            self._execute(delete, (username,))
            return True
        return False

    def _execute(self, *command):
        """ Internal helper function to shorten sqlite3 cursor.execute() code.

        Args:
            *command: string or parameterized tuple of arbitrary length, works
                identically to parameter(s) of sqlite3's cursor.execute().
        Return:
            Sqlite3 cursor after executing and committing passed *command.
        """
        conn = sqlite3.connect(self.location)
        cur = conn.cursor()
        cur.execute(*command)
        conn.commit()
        return cur
