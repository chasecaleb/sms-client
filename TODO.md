# TODO

* UI
    * Login/interact with server
    * Implement ConversationFrame footer (i.e. send box)
    * Keep current thread on side panel highlighted when main panel focused
    * Show notifications
    * Left/right align text in conversations, indent wrapped text
    * Config menu (keybinds, server address, notification type)
* Android App
    * Upload (fix) deleted messages
        * Name + address for contact
        * Date + address for message
    * Upload MMS
* Misc
    * Clean up docstrings
    * Clean up unit tests
    * Create integration tests
