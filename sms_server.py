#!/usr/bin/python
import socketserver
import threading
import urllib.request
import json
import time
from user_authentication import UserAuthentication
from sms_database import Messages, Contacts


class MyHandler(socketserver.BaseRequestHandler):

    def handle(self):
        """ SocketServer handler for client backend and database messages. """
        received_raw = self.request.recv(1024).decode().strip()
        try:
            received = json.loads(received_raw)
            # Both client and database are authenticated in case of spoofing.
            if self._authenticate(received):
                if received["from"] == "client":
                    message = self._handle_client(received)
                elif received["from"] == "database":
                    message = self._handle_database(received)
            else:
                message = json.dumps({"category": "unauthorized"})
        except (ValueError, KeyError):
            message = json.dumps({"category": "error"})
        if message:
            self.request.send(message.encode())

    def _authenticate(self, received):
        """ Helper function for handle(). 

        Authenticates via password or session_id.
        """
        if "password" in received:
            return self.server.user_auth.authenticate(
                received["username"], password=received["password"])
        elif "session_id" in received:
            return self.server.user_auth.authenticate(
                received["username"], session_id=received["session_id"])
        else:
            raise KeyError

    def _handle_client(self, received):
        """ Helper function for handle(). 

        KeyError and ValueError should be caught by caller.

        Return:
            String (via json.dumps() on dict) to be sent to client.
        """
        if received["category"] == "login":
            new_instance = Client(self.server.user_auth, received["username"])

            message_dict = {"category": "authenticated"}
            if "password" in received:
                session_id = self.server.user_auth.create_session_id(
                    received["username"])
                message_dict["session_id"] = session_id
            else:
                session_id = received["session_id"]
            message = json.dumps(message_dict)

            self.server.instances[session_id] = new_instance
        else:
            session_id = received["session_id"]
            self.server.instances[session_id].handle_received(received)
            message = self.server.instances[session_id].pop_queue()
        return message

    def _handle_database(self, received):
        """ Helper function for handle(). 

        Queue notification for any session instances matching
        received["username"].

        Return:
                None. Database doesn't need/want a response message.
        """
        sessions = (self.server.instances[i] for i in self.server.instances if
                    self.server.instances[i].username == received["username"])
        for s in sessions:
            # User doesn't care about password/gcm change notifications.
            if received["category"] not in (
                    "change_gcm_id", "change_password"):
                s.queue.append(received)


class Client:

    GOOGLE_API_KEY = "AIzaSyCJO8F4ZKf_TcanbnxdeInZ5zUf6oX7SZs"

    def __init__(self, user_auth, username):
        self.user_auth = user_auth
        database_filename = self.user_auth.get_sms_database(username)
        self.messages = Messages(database_filename)
        self.contacts = Contacts(database_filename)
        self.username = username
        self.queue = []

    @property
    def google_cloud_id(self):
        return self.user_auth.get_google_cloud_id(self.username)

    def handle_received(self, received):
        try:
            switch = {
                "sync_sms": lambda: self._sync_sms(received["msg_id"]),
                "sync_contacts": self._sync_contacts,
                "sms": lambda: self.send_sms(
                    received["address"], received["body"])
            }
            switch[received["category"]]()
        except KeyError:
            pass

    def pop_queue(self):
        """ Return type is string, not json dict. """
        default = {"category": "pong"}
        message = self.queue.pop(0) if self.queue else default
        return json.dumps(message)

    def send_sms(self, address, body):
        url = "https://android.googleapis.com/gcm/send"
        header = {"Authorization": "key=" + self.GOOGLE_API_KEY,
                  "Content-Type": "application/json"}
        content_raw = {"registration_ids": [self.google_cloud_id], 
                       "data": {"command": "send", "address": address, 
                                "body": body}
                       }
        content = json.dumps(content_raw)

        request = urllib.request.Request(url, data=content, headers=header)
        urllib.request.urlopen(request)

    def _sync_sms(self, starting_msg_id):
        """ Append messages newer than starting_msg_id to queue. """
        for msg in self.messages:
            if msg.msg_id > int(starting_msg_id):
                queue_item = vars(msg)
                queue_item["category"] = "sms"
                self.queue.append(queue_item)

    def _sync_contacts(self):
        """ Append all contacts to self.queue. """
        for contact in self.contacts:
            queue_item = vars(contact)
            queue_item["category"] = "contact"
            self.queue.append(queue_item)


def create_server(poll_time=0.5, user_database="users.db"):
    """ Shortcut to create custom threaded SocketServer.

    Start threaded SocketServer with custom handler class.
    Call shutdown() and server_close() on returned object to quit.

    Return:
        SocketServer.TCPServer() instance.
    """
    PORT = 55100
    socketserver.TCPServer.allow_reuse_address = True
    server = socketserver.TCPServer(("", PORT), MyHandler)
    server.user_auth = UserAuthentication(user_database)
    server.instances = {}

    t = threading.Thread(target=server.serve_forever, args=(poll_time,))
    t.setDaemon(True)
    t.start()
    return server

if __name__ == "__main__":
    server = create_server()
    print("Serving")
    while True:
        try:
            # TODO: delete idle instance
            # (server.instances[i].last_activity < time.time() - ___ time)
            time.sleep(1)
        except KeyboardInterrupt:
            print("\n Quitting.")
            server.shutdown()
            server.server_close()
            break
