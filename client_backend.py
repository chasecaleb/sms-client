#!/usr/bin/python
import threading
import socket
import json
import time
from sms_database import Messages, Contacts
from client_config import ClientConfig


# TODO: reorganize mess of functions
class ClientBackend(threading.Thread):
    """ Backend for UI, with communcation via queue.Queue()'s.

    - Provides UI with interface to server, config settings, and sms database.
    - The only functions that should be called directly are __init__() and
    start(). Interact with UI using passed Queue objects; server interaction
    handled automatically.
    - To stop thread: set self.do_run to False.

    Args:
        from_ui, to_ui: queue.Queue() objects for UI communication.
        config: Location of ClientConfig conf file (will create if
        missing). Default = "client.conf".
    """

    def __init__(self, from_ui, to_ui, config="client.conf"):
        super(ClientBackend, self).__init__()
        self.from_ui, self.to_ui = from_ui, to_ui
        self._config_location = config
        self.daemon = True
        self.do_run = True
        self.is_authenticated = False
        self.send_queue = []

    def _setup(self):
        self.config = ClientConfig(self._config_location)
        self.messages = Messages(self.config.database_location)
        self.contacts = Contacts(self.config.database_location)

    def run(self):
        """ Send sync request to server, then loop until self.do_run is False.

        Handles communication with server and UI.
        """
        self._setup()

        self.login(self.config.username)
        # Sync messages right away
        last_msg = self.messages[-1].msg_id
        self.send_queue.append({"category": "sync_sms", "msg_id": last_msg})
        while self.do_run:
            received = self._communicate_with_server()
            if received is not None:
                self._handle_server(received)
            if not self.from_ui.empty():
                ui_message = self.from_ui.get()
                self._handle_ui(ui_message)
            else:  # Avoid killing cpu while idle
                time.sleep(0.1)

    def _communicate_with_server(self, message=None):
        """ Send next message in self.send_queue (or optionally passed message)
        to server and get response.

        Return:
            Response from server (dictionary) or None if error occured.
        """
        if message is None:
            message = self._pop_queue()
            # Client not authenticated with server, no message to send.
            if message is None:
                return None

        message = json.dumps(message)
        try:
            with socket.create_connection(self.config.server_info) as s:
                s.send(message.encode())
                received = s.recv(1024).decode()
                try:
                    return json.loads(received)
                except ValueError:
                    pass
        except OSError:
            return None

    def _handle_server(self, received):
        """ Handle received message, then notify UI.

        Args:
            received: dictionary from server. Examples:
                - {"category": "pong"}
                - {"category": "authenticated"}
                - {"category": "sms", "msg_id": int, "thread_id":  int,
                "address": string, "date": int, "msg_type": int, "body":
                string}
                - {"category": "contact", "lookup": string, "name": string,
                "address": string}
                - {"category": "deleted_contact", "lookup": string}
        """
        save_sms = lambda: self.messages.append([
            received[key] for key in ("msg_id", "thread_id", "address", "date",
                                      "msg_type", "body")])
        save_contact = lambda: self.contacts.append([
            received[k] for k in ("lookup", "name", "address")])

        switch = {
            "pong": lambda: time.sleep(1),
            "authenticated": lambda: setattr(self, "is_authenticated", True),
            "unauthorized": lambda: setattr(self, "is_authenticated", False),
            "sms": save_sms,
            "contact": save_contact,
            "deleted_contact": lambda: self.contacts.remove(
                received["lookup"]),
        }

        category = received["category"]
        switch[category]()
        if category != "pong":
            self.to_ui.put([None, received])

    def _handle_ui(self, item):
        """ Handle item from ui queue: call function or get/set variable.

        Response is put into queue for UI, not returned.
        NOTE: "self." is added to start of function/variable name, do not add
        it yourself. E.G. "self.variable" is just "variable" and
        "self.child.variable" is "child.variable".

        Args:
            item examples:
                call with args: ["parent.func_name", "arg 1", "arg 2"]
                call with no args: "parent.func_name" or ["parent.func_name"]
                get variable: "parent.var_name"
                set variable: ["parent.var_name", "value"]
        Return:
            None (response goes into UI queue).
        """
        def _getattr(attr_name, stack=self):
            """ Convert attr_name to attribute/function reference.
            I.E. 'value' -> self.value, 'child.value' -> self.child.value.
            """
            if not attr_name:
                return stack
            try:
                head, tail = attr_name.split('.', maxsplit=1)
            except ValueError:
                head, tail = attr_name, []
            return _getattr(tail, getattr(stack, head))

        try:
            callback, attr_name, *params = item
            attr = _getattr(attr_name)

            if callable(attr):
                output = attr(*params) if params else attr()
            else:
                if params:
                    # Set either self or child attributes
                    # (Only child attributes have a period in attr_name)
                    try:
                        parent_name, child_name = attr_name.rsplit(
                            '.', maxsplit=1)
                    except ValueError:
                        setattr(self, attr_name, params[0])
                    else:
                        setattr(_getattr(parent_name), child_name, params[0])
                    output = True
                else:
                    output = attr
        except AttributeError:
            output = False

        self.to_ui.put([callback, output])

    def send_sms(self, address, body):
        message = {"category": "sms", "address": address, "body": body}
        self.send_queue.append(message)

    def sync(self):
        self.send_queue.append({
            "category": "sync_sms", "msg_id": self.messages[-1].msg_id})
        self.send_queue.append({"category": "sync_contacts"})

    def login(self, username, password=None):
        """ Attempt to login via password if passed, otherwise session_id. """
        # Automatically save username when user tries to login.
        self.config.username = username
        message = {"from": "client", "category": "login", "username": username}
        if password:
            message["password"] = password
        else:
            if self.config.session_id:
                message["session_id"] = self.config.session_id
            else:
                return False

        response = self._communicate_with_server(message=message)
        if response is None:  # Communication error.
            return False
        elif response["category"] == "authenticated":
            self.is_authenticated = True
            # Server sends session_id in response to valid password login.
            if "session_id" in response:
                self.config.session_id = response["session_id"]
        else:
            self.is_authenticated = False
        return self.is_authenticated

    def _pop_queue(self):
        """ Remove and return next element from queue (placeholder if empty).

        Return:
            If self.config.username/self.config.password unset: Abort and
            return none.
            If queue is empty: Default dictionary containing ping.
            Otherwise: Dictionary of next element with from, username, and
            password keys inserted.
        """
        # Presumably username and session_id are set if authenticated
        if self.is_authenticated:
            message = {"from": "client", "username": self.config.username,
                       "session_id": self.config.session_id}
            if self.send_queue:
                message.update(self.send_queue.pop(0))
            else:
                message.update({"category": "ping"})
            return message
        return None
