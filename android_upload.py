#!/usr/bin/python
import cgi
import socket
import json
from user_authentication import UserAuthentication
from sms_database import Messages, Contacts


def main(user_database=None, form=None):
    """
    Interpret CGI uploads from users phone, update database(s), and notify
    sms_server. Uses handle_upload() and notify() helper functions.

    Args:
        user_database: Location of database file used by UserAuthentication(),
        leave empty for default.
        form: Leave empty to use cgi.FieldStorage, pass dictionary for
        testing.
    Return:
        - Success: "Content-type:text/html\r\n"
        - Error: HTTP error strings -- see handle_upload().
    """
    # For temporary testing with Android emulator.
    # user_auth = UserAuthentication("tests/helpers/mock_users.db")
    if user_database:
        user_auth = UserAuthentication(user_database)
    else:
        user_auth = UserAuthentication()
    if not form:
        form = cgi.FieldStorage()

    result = handle_upload(user_auth, form)
    if result is not True:
        return result
    else:
        notification = {key: form[key] for key in iter(form)}
        notification["from"] = "database"
        notify(notification)
        return "Content-type:text/html\r\n"


def handle_upload(user_auth, form):
    """
    Update sms and user databases based on passed form.

    Args:
        form: cgi.FieldStorage() instance (or dictionary variable).
        user_auth: user_authentication.UserAuthentication() instance.
    Return:
        - True if handled successfully
        - String with HTTP status if not.
            - "Status: 400 Bad Request\r\n\r\n" if missing form key
            - "Status: 401 Unauthorized\r\n\r\n" if invalid username/password.
    """
    try:
        if form["category"] == "create_account":
            return _create_account(user_auth, form)

        sms_location = user_auth.get_sms_database(form["username"])
        messages = Messages(sms_location)
        contacts = Contacts(sms_location)

        # Important! Functions past here assume user is who they say they are.
        if not user_auth.authenticate(form["username"], form["password"]):
            return "Status: 401 Unauthorized\r\n\r\n"

        # TODO: de-uglify
        category = form["category"]
        if category == "change_password":
            user_auth.change_password(form["username"], form["new_password"])
        elif category == "change_gcm_id":
            user_auth.change_google_cloud_id(form["username"], form["gcm_id"])
        elif category == "sms":
            new_message = [form["msg_id"], form["thread_id"], form["address"],
                           form["date"], form["msg_type"], form["body"]]
            messages.append(new_message)
        elif category == "contact":
            new_contact = [form["lookup"], form["name"], form["address"]]
            contacts.append(new_contact)
        elif category == "deleted_contact":
            contacts.remove(form["lookup"])
            # del contacts[form["lookup"]]
    except KeyError:
        return "Status: 400 Bad Request\r\n\r\n"
    return True


def notify(info):
    """
    Send info (json object) to TCP server.
    NOTE: fails silently if unable to connect to socket.
    """
    address = ("localhost", 55100)
    json_info = json.dumps(info)
    try:
        with socket.create_connection(address) as s:
            s.send(json_info)
    except OSError:
        pass


def _create_account(user_auth, form):
    """ Helper for handle_upload(). Attempts to create new user account. """
    try:
        created = user_auth.create_account(form["username"], form["password"],
                                           form["gcm_id"])
    except KeyError:
        created = False
    return True if created else "Status: 400 Bad Request\r\n\r\n"

if __name__ == "__main__":
    response = main()
    print(response)
