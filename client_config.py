#!/usr/bin/python

class BaseConfig:

    """ Hybrid variable/file storage system for config properties.

    Reads property from config file, then caches as internal _variable.
    Updates internal variable and saves to file when a property is modified.

    Example implementation:
        @property
        def my_property(self):
            return self.get_value("my_property", default="placeholder")
        @my_property.setter
        def my_property(self, value):
            self._my_property = value
            self.store_value(value)
    """

    def __init__(self, location):
        """ Location: filename string. """
        self.location = location

    def get_value(self, key, default=None):
        """ Try variable, then config file, then fallback to default.

        IMPORTANT: Returns a string if read from config file. Otherwise, will
        return as expected.
        """
        try:
            internal_name = "_" + key
            value = getattr(self, internal_name)
        except AttributeError:
            value = self._read_value_from_file(key)
        return value if value else default

    def store_value(self, key, value):
        """ Save to file. Overwrite identical key, if one exists. """
        new_line = str(key) + ":" + str(value) + "\n"
        try:
            config = []
            with open(self.location, "r+") as f:
                config = [new_line if line.startswith(key) else line for line
                        in f]
                if new_line not in config:
                    config.append(new_line)
            with open(self.location, "w") as f:
                f.writelines(config)
        except FileNotFoundError:
            with open(self.location, "w") as f:
                f.write(new_line)

    def _read_value_from_file(self, key):
        """ Helper for get_value().
        
        Return string of value matching key if found, else None.
        """
        try:
            with open(self.location, "r") as f:
                for line in f:
                    if line.startswith(key):
                        return line.strip().split(":")[-1]
        except FileNotFoundError:
            pass


class ClientConfig(BaseConfig):
    """ Subclass of BaseConfig. Contains variables for client usage. """

    def __init__(self, location="client.conf"):
        super().__init__(location)
        # password is not persistent (i.e. not stored in config file)
        self.password = None

    @property
    def database_location(self):
        """ Location of sms database.

        Default: "sms.db".
        """
        return self.get_value("database_location", default="sms.db")
    @database_location.setter
    def database_location(self, value):
        self._database_location = value
        self.store_value("database_location", value)

    @property
    def server_info(self):
        """ Tuple containing address (string) and port (int) of server.

        Default: ("localhost", 55100).
        """
        address = self.get_value("server_address", default="localhost")
        port = int(self.get_value("server_port", default=55100))
        return (address, port)
    @server_info.setter
    def server_info(self, value):
        self._server_info = value
        self.store_value("server_address", value[0])
        self.store_value("server_port", value[1])

    @property
    def username(self):
        """ Default: None. """
        return self.get_value("username")
    @username.setter
    def username(self, value):
        self._username = value
        self.store_value("username", value)

    @property
    def session_id(self):
        return self.get_value("session_id")
    @session_id.setter
    def session_id(self, value):
        self._session_id = value
        self.store_value("session_id", value)
