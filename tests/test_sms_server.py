import unittest
import unittest.mock
import socket
import json
from sms_server import Client
from user_authentication import UserAuthentication
from tests import my_setup


class TestHandler(unittest.TestCase):

    def setUp(self):
        my_setup.setup_files(self)
        # Calls sms_server.create_server and handles proper cleanup
        self.server = my_setup.start_server_with_cleanup(self)
        self.msg_template = {"from": "client", "username": "user@fake.com",
                             "password": "password", "category": "login"}
        self._setup_bcrypt_checkpw_mock()

    def _setup_bcrypt_checkpw_mock(self):
        def side_effect(*args):
            return (args[0] == "password")
        mock_checkpw = unittest.mock.MagicMock(side_effect=side_effect)
        patcher = unittest.mock.patch("bcrypt.checkpw", mock_checkpw)
        self.addCleanup(patcher.stop)
        self.mock_checkpw = patcher.start()

    def _send_and_receive(self, message):
        address = ("localhost", 55100)
        with socket.create_connection(address) as client:
            client.send(message.encode())
            response = client.recv(1024).decode()
        try:
            return json.loads(response)
        except ValueError:
            pass

    def test_handle_sends_response_on_value_or_key_error(self):
        del self.msg_template["password"]
        message = json.dumps(self.msg_template)
        response = self._send_and_receive(message)
        self.assertDictEqual({"category": "error"}, response)

    def test_handle_sends_unauthorized_response(self):
        self.msg_template["password"] = "invalid"
        message = json.dumps(self.msg_template)
        response = self._send_and_receive(message)
        self.assertEqual({"category": "unauthorized"}, response)

    def test_handle_client_login_via_password(self):
        """ Check that server creates instance and sends new session_id. """
        self.assertTrue("user@fake.com" not in self.server.instances)
        message = json.dumps(self.msg_template)
        response = self._send_and_receive(message)
        self.assertIn(response["session_id"], self.server.instances)

    def test_handle_client_login_via_session_id(self):
        """ Check that server response doesn't generate new session_id. """
        self.assertTrue("user@fake.com" not in self.server.instances)
        del self.msg_template["password"]
        self.msg_template["session_id"] = "12345"
        message = json.dumps(self.msg_template)
        expected = {"category": "authenticated"}
        self.assertDictEqual(expected, self._send_and_receive(message))

    def test_handle_client_password_login_replies_with_session_id(self):
        message = json.dumps(self.msg_template)
        mock_uuid = unittest.mock.MagicMock(return_value="12345")
        with unittest.mock.patch("uuid.uuid4", mock_uuid):
            response = self._send_and_receive(message)
        expected = {"category": "authenticated", "session_id": "12345"}
        self.assertEqual(expected, response)

    def test_handle_client_password_login_replies_without_session_id(self):
        pass

    def test_authenticate_session_id(self):
        """ Get session_id by logging in with password, then check that
        session_id authenticates properly.
        """
        message_one = json.dumps(self.msg_template.copy())
        session_id = self._send_and_receive(message_one)["session_id"]
        message_two = {"from": "client", "username": "user@fake.com",
                       "session_id": session_id, "category": "placeholder"}
        received = self._send_and_receive(json.dumps(message_two))
        self.assertEqual({"category": "pong"}, received)

    def test_handle_pops_queue_from_existing_instance(self):
        # call first time to create instance
        session_id = self._send_and_receive(
            json.dumps(self.msg_template))["session_id"]
        self.server.instances[session_id].queue.append({"test": ""})
        self.msg_template["category"] = "placeholder"
        self.msg_template["session_id"] = session_id
        message_two = json.dumps(self.msg_template)
        response = self._send_and_receive(message_two)
        self.assertDictEqual({"test": ""}, response)

    def test_handle_ignores_database_if_instance_doesnt_exist(self):
        message = json.dumps({"from": "database", "username": "user@fake.com",
                              "password": "password"})
        self._send_and_receive(message)
        self.assertNotIn("user@fake.com", self.server.instances)

    def test_handle_appends_database_message_to_client_queue(self):
        response = self._send_and_receive(json.dumps(self.msg_template))
        session_id = response["session_id"]
        database_json = {"from": "database", "category": "placeholder",
                         "username": "user@fake.com", "password": "password"}
        database_message = json.dumps(database_json)
        self._send_and_receive(database_message)
        self.assertIn(database_json, self.server.instances[session_id].queue)


class TestClient(unittest.TestCase):

    def setUp(self):
        my_setup.setup_files(self)
        self.logic = Client(UserAuthentication(my_setup.user_file),
                            "user@fake.com")

    def test_google_cloud_id_property_updates_on_changes(self):
        self.logic.user_auth.change_google_cloud_id("user@fake.com", "new-key")
        self.assertEqual("new-key", self.logic.google_cloud_id)

    def test_pop_queue_returns_pong_when_queue_empty(self):
        result = json.loads(self.logic.pop_queue())
        self.assertEqual({"category": "pong"}, result)

    def test_pop_queue_returns_item_when_not_empty(self):
        expected = {"fake": "fake_value"}
        self.logic.queue.append(expected)
        result = json.loads(self.logic.pop_queue())
        self.assertDictEqual(expected, result)

    def test_handle_sync_sms_appends_two_messages(self):
        message = {"from": "client", "category": "sync_sms", "msg_id": "5"}
        self.logic.handle_received(message)
        expected = [{"msg_id": i[0], "thread_id": i[1], "address": i[2],
                     "date": i[3], "msg_type": i[4], "body": i[5], 
                     "category": "sms"}
                    for i in my_setup.messages if i[0] > 5]
        self.assertListEqual(expected, self.logic.queue)

    def test_handle_sync_sms_no_new_messages(self):
        message = {"from": "client", "category": "sync_sms", "msg_id": "900"}
        self.logic.handle_received(message)
        self.assertListEqual([], self.logic.queue)

    def test_handle_sync_contacts_appends_to_queue(self):
        message = {"from": "client", "category": "sync_contacts"}
        self.logic.handle_received(message)
        expected = [{"lookup": i[0], "name": i[1], "address": i[2], 
                     "category": "contact"} for i in my_setup.contacts]
        self.assertListEqual(expected, self.logic.queue)

    def test_handle_received_sends_sms(self):
        message = {"category": "sms", "address": "8885550000", "body": 
                   "testing 123"}
        url = "https://android.googleapis.com/gcm/send"
        header = {"Authorization": "key=" + self.logic.GOOGLE_API_KEY,
                  "Content-Type": "application/json"}
        data = json.dumps({
            "registration_ids": [self.logic.google_cloud_id],
            "data": {"command": "send", "address": message["address"], "body":
                     message["body"]}
        })

        mock_request = unittest.mock.MagicMock()
        with unittest.mock.patch("urllib.request", mock_request):
            self.logic.handle_received(message)
        mock_request.Request.assert_called_once_with(
            url, data=data, headers=header)


if __name__ == "__main__":
    unittest.main()
