import unittest
import unittest.mock
import time
import queue
from client_backend import ClientBackend
from tests import my_setup


class TestClientBackend(unittest.TestCase):

    def setUp(self):
        my_setup.setup_files(self)
        self.sleep_time = 0.01
        self.from_ui, self.to_ui = queue.Queue(), queue.Queue()
        self.backend = ClientBackend(
            self.from_ui, self.to_ui, my_setup.client_file)
        self.backend._setup()

    def _callback(self):
        """ Dummy function used to test UI callback. """
        pass

    def test_thread_startup_calls_run_function(self):
        mock_run = unittest.mock.MagicMock()
        with unittest.mock.patch("client_backend.ClientBackend.run", mock_run):
            self.backend.start()
            time.sleep(self.sleep_time)
        mock_run.assert_called_once_with()

    """ _handle_ui() misc """
    def test_get_from_ui_queue(self):
        """ Ensure that input/output queues aren't swapped. """
        expected = "test"
        self.from_ui.put(expected)
        result = self.backend.from_ui.get()
        self.assertEqual(expected, result)

    def test_handle_ui_sync_with_server(self):
        """
        Send two messages to server: one to sync messages newer than most
        recent in local database, another to sync all contacts.
        """
        self.backend._handle_ui([self._callback, "sync"])
        expected_one = {"category": "sync_sms", "msg_id": 7}
        expected_two = {"category": "sync_contacts"}
        self.assertIn(expected_one, self.backend.send_queue)
        self.assertIn(expected_two, self.backend.send_queue)

    def test_handle_ui_send_sms(self):
        message = [self._callback, "send_sms", "8885551234", "test msg"]
        expected = {"category": "sms", "address": "8885551234", "body":
                    "test msg"}
        self.backend._handle_ui(message)
        self.assertIn(expected, self.backend.send_queue)

    def test_handle_ui_get_authenticated_variable(self):
        self.backend.is_authenticated = True
        self.backend._handle_ui([self._callback, "is_authenticated"])
        expected = [self._callback, True]
        self.assertListEqual(expected, self.to_ui.get_nowait())

    """ _handle_ui() config """
    def test_handle_ui_config_get_invalid_attribute_sends_error(self):
        self.backend._handle_ui([self._callback, "config.fake_attr"])
        expected = [self._callback, False]
        self.assertListEqual(expected, self.to_ui.get_nowait())

    def test_handle_ui_config_set_invalid_attribute_sends_error(self):
        """ Prevent typos/stupidity: don't let UI create attributes. """
        self.backend._handle_ui(
            [self._callback, "config.fake_attr", "new value"])
        self.assertRaises(AttributeError,
                          lambda: getattr(self.backend.config, "fake_attr"))
        expected = [self._callback, False]
        self.assertEqual(expected, self.to_ui.get_nowait())

    def test_handle_ui_config_set_location(self):
        self.backend._handle_ui(
            [self._callback, "config.location", "new_config"])
        self.assertEqual(self.backend.config.location, "new_config")
        expected = [self._callback, True]
        self.assertEqual(expected, self.to_ui.get_nowait())

    def test_handle_ui_config_get_location(self):
        self.backend._handle_ui([self._callback, "config.location"])
        expected = [self._callback, my_setup.client_file]
        self.assertEqual(expected, self.to_ui.get_nowait())

    """ _handle_ui() login """
    def test_handle_ui_login_via_password(self):
        """
        Should  set is_authenticated, set config.session_id, and put
        response in ui queue.
        """
        my_setup.start_server_with_cleanup(self)
        mock_uuid = unittest.mock.MagicMock(return_value="12345")
        with unittest.mock.patch("uuid.uuid4", mock_uuid):
            self.backend._handle_ui(
                [self._callback, "login", "user@fake.com", "password"])
        self.assertTrue(self.backend.is_authenticated)
        self.assertEqual("12345", self.backend.config.session_id)
        expected = [self._callback, True]
        self.assertEqual(expected, self.to_ui.get_nowait())

    def test_handle_ui_login_via_session_id(self):
        """ Should set is_authenticated and notify ui. """
        my_setup.start_server_with_cleanup(self)
        self.backend._handle_ui([self._callback, "login", "user@fake.com"])
        self.assertTrue(self.backend.is_authenticated)
        expected = [self._callback, True]
        self.assertEqual(expected, self.to_ui.get_nowait())

    def test_login_server_error_returns_false(self):
        """ Server isn't running, so login should fail. """
        self.assertEqual(False, self.backend.login("user@fake.com"))

    """ _handle_ui() database -- minimally tested """
    def test_handle_ui_get_contacts(self):
        self.backend._handle_ui([self._callback, "contacts"])
        expected = [self._callback, self.backend.contacts]
        self.assertEqual(expected, self.to_ui.get_nowait())

    """ _pop_queue() """
    def test_pop_queue_returns_none_if_username_password_not_set(self):
        self.assertIsNone(self.backend._pop_queue())

    def test_pop_queue_returns_ping_when_empty(self):
        self.backend.config.username = "user@fake.com"
        self.backend.password = "password"
        self.backend.is_authenticated = True
        expected = {"from": "client", "category": "ping", "username":
                    "user@fake.com", "session_id": "12345"}
        result = self.backend._pop_queue()
        self.assertDictEqual(expected, result)

    def test_pop_queue_returns_message(self):
        self.backend.config.username = "user@fake.com"
        self.backend.config.session_id = "fakesession"
        self.backend.is_authenticated = True
        sms = {"category": "sms", "address": "8885550000", "body":
               "test message"}
        expected = sms.copy()
        expected.update({"from": "client", "username": "user@fake.com",
                         "session_id": "fakesession"})
        self.backend.send_queue.append(sms)
        result = self.backend._pop_queue()
        self.assertDictEqual(expected, result)

    """ _handle_server() """
    def test_receive_pong_calls_sleep(self):
        mock_sleep = unittest.mock.MagicMock()
        with unittest.mock.patch("time.sleep", mock_sleep):
            self.backend._handle_server({"category": "pong"})
        mock_sleep.assert_called_once_with(1)

    def test_receive_pong_does_not_notify_ui(self):
        mock_sleep = unittest.mock.MagicMock()
        with unittest.mock.patch("time.sleep", mock_sleep):
            self.backend._handle_server({"category": "pong"})
        self.assertTrue(self.backend.to_ui.empty())

    def test_receive_authenticated_message_set_variable_to_true(self):
        message = {"category": "authenticated"}
        self.backend._handle_server(message)
        self.assertTrue(self.backend.is_authenticated)

    def test_handle_server_sms_inserts_into_database(self):
        expected = {"msg_id": 999, "thread_id": 2, "address": "8885550000",
                    "date": 1392934962000, "msg_type": 2, "body": "test msg"}
        message = expected.copy()
        message["category"] = "sms"
        self.assertNotEqual(expected, self.backend.messages[-1]._asdict())
        self.backend._handle_server(message)
        self.assertDictEqual(expected, self.backend.messages[-1]._asdict())

    def test_handle_server_sms_notifies_ui(self):
        """ Put [None, message] in queue so that UI uses default callback. """
        message = {"category": "sms", "msg_id": 999, "thread_id": 2, "address":
                   "8885550000", "date": 1392934962000, "msg_type": 2, "body":
                   "test msg"}
        self.backend._handle_server(message)
        expected = [None, message]
        self.assertListEqual(expected, self.backend.to_ui.get_nowait())

    def test_handle_server_contact_saves_in_database(self):
        contact = {"lookup": "12345", "name": "someone", "address":
                   "8885554444"}
        message = contact.copy()
        message["category"] = "contact"
        self.assertNotEqual(contact, self.backend.contacts[-1]._asdict())
        self.backend._handle_server(message)
        self.assertDictEqual(contact, self.backend.contacts[-1]._asdict())

    def test_handle_server_delete_contact_removes_from_database(self):
        contact = my_setup.contacts[0]
        message = {"category": "deleted_contact", "lookup": contact[0]}
        self.assertTupleEqual(contact, self.backend.contacts[0])
        self.backend._handle_server(message)

if __name__ == "__main__":
    unittest.main()
