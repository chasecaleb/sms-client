import unittest
from client_config import ClientConfig
# from tests import boilerplate

@unittest.skip("")
class TestClientConfig(unittest.TestCase):
    """ Full tests done for database_location property, rest are minimal. """

    def setUp(self):
        boilerplate.setup_files(self)
        self.config = ClientConfig(self.client_file)

    def _get_contents(self):
        """ Returns array of *stripped* lines in file. """
        with open(self.client_file) as f:
            return [line.strip() for line in f]

    """ database_location """
    def test_get_database_location_returns_internal_value_if_set(self):
        self.config._database_location = "internal"
        self.assertEqual("internal", self.config.database_location)

    def test_get_database_location_returns_from_file_if_not_set(self):
        self.assertEqual(self.sms_file, self.config.database_location)

    def test_get_database_location_returns_default_if_not_set_and_no_file(self):
        self.config.location = "fake"
        self.assertEqual("sms.db", self.config.database_location)

    def test_set_database_location_saves_to_file_and_sets_internal_var(self):
        self.assertNotEqual("new_file", self.config.database_location)
        self.config.database_location = "new_file"
        self.assertIn("database_location:new_file", self._get_contents())
        self.assertEqual("new_file", self.config._database_location)

    """ server_info """
    def test_get_server_info_returns_saved(self):
        self.assertTupleEqual(("127.0.0.1", 55100), self.config.server_info)

    def test_set_server_info_saves_to_file_and_sets_internal_var(self):
        self.config.server_info = ("an ip", 123)
        contents = self._get_contents()
        self.assertIn("server_address:an ip", contents)
        self.assertIn("server_port:123", contents)
        self.assertTupleEqual(("an ip", 123), self.config._server_info)

    """ username """
    def test_get_username_returns_saved(self):
        self.assertEqual("user@fake.com", self.config.username)

    def test_set_username_updates_internal_variable_and_file(self):
        self.config.username = "new"
        self.assertEqual("new", self.config._username)
        self.assertIn("username:new", self._get_contents())

    """ session_id """
    def test_get_session_id_returns_saved(self):
        self.assertEqual("fakehash", self.config.session_id)
    def test_set_session_id_updates_internal_variable_and_file(self):
        self.config.session_id = "newhash"
        self.assertEqual("newhash", self.config._session_id)
        self.assertIn("session_id:newhash", self._get_contents())
