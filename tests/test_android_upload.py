import unittest
import unittest.mock
import android_upload
from user_authentication import UserAuthentication
from sms_database import Messages, Contacts
from tests import my_setup


class TestAndroidSms(unittest.TestCase):

    # Set to True to run slow tests (because of crypto in user_authentication)
    run_full_tests = False

    def setUp(self):
        my_setup.setup_files(self)
        self.user_auth = UserAuthentication(my_setup.user_file)
        self.messages = Messages(my_setup.sms_file)
        self.contacts = Contacts(my_setup.sms_file)
        self.cgi_values = {
            "username": "user@fake.com",
            "password": "password",
            "gcm_id": "gcm_key_abc"
        }

    def test_main_returns_content_type_for_valid_upload(self):
        self.cgi_values["category"] = "change_gcm_id"
        self.cgi_values["gcm_id"] = "12345"
        result = android_upload.main(my_setup.user_file, self.cgi_values)
        self.assertEqual("Content-type:text/html\r\n", result)

    def test_main_returns_result_from_handle_upload_if_not_true(self):
        result = android_upload.main(my_setup.user_file, self.cgi_values)
        self.assertEqual("Status: 400 Bad Request\r\n\r\n", result)

    def test_main_call_notify_with_from_key_added(self):
        mock_notify = unittest.mock.MagicMock()
        mock_handle = unittest.mock.MagicMock(return_value=True)
        expected = self.cgi_values.copy()
        expected["from"] = "database"
        with unittest.mock.patch("android_upload.notify", mock_notify):
            with unittest.mock.patch("android_upload.handle_upload",
                                     mock_handle):
                android_upload.main(self.user_auth, self.cgi_values)
        mock_notify.assert_called_once_with(expected)

    def test_handle_upload_returns_false_for_missing_key(self):
        """ 
        Make sure missing form["category"] key (etc.) doesn't cause crash.
        """
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertEqual("Status: 400 Bad Request\r\n\r\n", result)

    def test_handle_upload_to_create_valid_account(self):
        """ Return true when successful. """
        self.cgi_values["category"] = "create_account"
        self.cgi_values["username"] = "new_user@fake.com"
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertTrue(result)

    def test_handle_upload_create_preexisting_account_returns_error(self):
        """
        Return error string when trying to create account with username that
        already exists.
        """
        self.cgi_values["category"] = "create_account"
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertEqual("Status: 400 Bad Request\r\n\r\n", result)

    def test_handle_upload_create_account_when_missing_key(self):
        self.cgi_values["category"] = "create_account"
        del self.cgi_values["username"]
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertEqual("Status: 400 Bad Request\r\n\r\n", result)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_handle_upload_unauthorized_user(self):
        self.cgi_values["category"] = "sms"
        self.cgi_values["password"] = "invalid"
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertEqual("Status: 401 Unauthorized\r\n\r\n", result)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_handle_upload_change_password(self):
        self.cgi_values["category"] = "change_password"
        self.cgi_values["new_password"] = "new"
        self.assertTrue(android_upload.handle_upload(
            self.user_auth, self.cgi_values))
        self.assertTrue(self.user_auth.authenticate(
            self.cgi_values["username"], self.cgi_values["new_password"]))

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_handle_upload_change_gcm_id(self):
        self.cgi_values["category"] = "change_gcm_id"
        self.cgi_values["gcm_id"] = "123new_id"
        self.assertTrue(android_upload.handle_upload(
            self.user_auth, self.cgi_values))
        new_id = self.user_auth.get_google_cloud_id(
            self.cgi_values["username"])
        self.assertEqual("123new_id", new_id)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_handle_upload_containing_sms(self):
        self.cgi_values["category"] = "sms"
        sms = {"msg_id": 999, "thread_id": 1, "address": "8001234567", "date":
               1392934963102, "msg_type": 2, "body": "insert test"}
        self.cgi_values.update(sms)
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertTrue(result)
        actual = [i._asdict() for i in self.messages]
        self.assertIn(sms, actual)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_handle_upload_containing_contact(self):
        self.cgi_values["category"] = "contact"
        contact = {"lookup": "12345", "name": "someone", "address":
                   "8885554444"}
        self.cgi_values.update(contact)
        self.assertNotIn(contact, self.contacts)
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        actual = [i._asdict() for i in self.contacts]
        self.assertIn(contact, actual)
        self.assertTrue(result)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_handle_upload_containing_deleted_contact(self):
        self.cgi_values["category"] = "deleted_contact"
        contact = my_setup.contacts[0]
        self.cgi_values["lookup"] = contact[0]
        self.assertIn(contact, self.contacts)
        result = android_upload.handle_upload(self.user_auth, self.cgi_values)
        self.assertNotIn(contact, self.contacts)
        self.assertTrue(result)
