import shutil
import os
import sms_server


"""
Constants that I'm too lazy too keep on typing.
Make sure to run setup_files() to actually create temp files.
"""
sms_file = "tests/helpers/temp_sms.db"
user_file = "tests/helpers/temp_users.db"
client_file = "tests/helpers/temp_client.conf"

messages = [
    (1, 6, '8885551234', 1400378050079, 1, 'Received message one'),
    (2, 6, '(888) 555-1234', 1400378062843, 2, 'Test reply.'),
    (3, 6, '8885551234', 1400378076372, 1, 'Another message'),
    (4, 7, '(888) 555-4567', 1400378116627, 2, "Hey what's up?"),
    (5, 7, '8885554567', 1400378136431, 1, 
     'Just building a stupid test database...\nYup.'),
    (6, 7, '(888) 555-4567', 1400378146953, 2, 'Sounds fun.'),
    (7, 7, '8885554567', 1400378154113, 1, "Yes, I'm talking to myself.")
]
contacts = [
    ('0r4-273D4D2F492F3343', 'Alter Ego', '+18885554567'),
    ('0r5-2943293943412F4B', 'Bob Jones', '+18885556789')
]


def setup_files(self):
    """ Copy mock files to temp, add teardown. """
    shutil.copy("tests/helpers/mock_sms.db", sms_file)
    shutil.copy("tests/helpers/mock_users.db", user_file)
    shutil.copy("tests/helpers/mock_client.conf", client_file)
    self.addCleanup(lambda: os.remove(sms_file))
    self.addCleanup(lambda: os.remove(user_file))
    self.addCleanup(lambda: os.remove(client_file))


def start_server_with_cleanup(self):
    """ Start sms_server and add cleanup to kill it properly. """
    def stop_server(server):
        server.shutdown()
        server.server_close()

    server = sms_server.create_server(0.001, user_file)
    self.addCleanup(lambda: stop_server(server))
    return server
