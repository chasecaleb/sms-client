import unittest
import bcrypt
import hashlib
import sqlite3
from user_authentication import UserAuthentication
from tests import my_setup


class TestUserAuthentication(unittest.TestCase):

    # Set to True to run slow tests (because of bcrypt algorithms)
    run_full_tests = False

    def setUp(self):
        my_setup.setup_files(self)
        self.user_auth = UserAuthentication(my_setup.user_file)
        # pre-existing row for one user (to save typing)
        self.username = "user@fake.com"
        self.password = "password"
        self.google_cloud_id = "gcm-key-abc"

    """ create_account() """
    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_create_account(self):
        self.user_auth.create_account("fake@fake.com", "12345", "adcdef")
        db_filename = hashlib.sha256("fake@fake.com".encode()).hexdigest()
        conn = sqlite3.connect(my_setup.user_file)
        cur = conn.cursor()
        cur.execute("SELECT * FROM users WHERE username = 'fake@fake.com'")
        new_username, new_password, new_db, new_gcm_key = cur.fetchone()
        self.assertEqual("fake@fake.com", new_username)
        self.assertTrue(bcrypt.checkpw("12345", new_password))
        self.assertEqual(db_filename, new_db)
        self.assertEqual("adcdef", new_gcm_key)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_create_account_returns_false_if_username_exists(self):
        result = self.user_auth.create_account(
            self.username, "invalid", "invalid")
        self.assertEqual(False, result)

    """ authenticate() and create_session_id() """
    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_authenticate_returns_false_for_invalid_pass(self):
        result = self.user_auth.authenticate(self.username, "failpass")
        self.assertEqual(False, result)

    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_authenticate_returns_false_for_invalid_username(self):
        result = self.user_auth.authenticate(
            "nonexistant@fake.com", self.password)
        self.assertEqual(False, result)

    def test_authenticate_returns_true_for_valid_info(self):
        result = self.user_auth.authenticate(self.username, self.password)
        self.assertTrue(result)

    def test_authenticate_with_session_id(self):
        """
        Catches edge case where authentication fails by comparing
        number-as-string to integer and vice-versa.
        """
        self.assertTrue(self.user_auth.authenticate(
            "user@fake.com", session_id="12345"))
        self.assertTrue(self.user_auth.authenticate(
            "user@fake.com", session_id=12345))

    def test_create_session_id_and_authenticate_with_it(self):
        result = self.user_auth.create_session_id(self.username)
        self.assertEqual(36, len(result))
        self.assertTrue(self.user_auth.authenticate(
            self.username, session_id=result))

    def test_authenticate_with_invalid_session_id_returns_false(self):
        self.assertFalse(self.user_auth.authenticate(
            self.username, session_id="invalid"))

    def test_authenticate_with_expired_session_id_returns_false(self):
        statement = "REPLACE INTO sessions VALUES(?, ?, ?)"
        self.user_auth._execute(statement, (self.username, "new_id", 1))
        result = self.user_auth.authenticate(
            self.username, session_id="new_id")
        self.assertFalse(result)

    """ change_password() """
    @unittest.skipUnless(run_full_tests, "Skipping slow tests")
    def test_change_password_and_authenticate_with_new_one(self):
        update = self.user_auth.change_password(self.username, "newpass")
        authenticate = self.user_auth.authenticate(self.username, "newpass")
        self.assertTrue(update)
        self.assertTrue(authenticate)

    """ change_google_cloud_id() and get_google_cloud_id() """
    def test_get_google_cloud_id(self):
        expected = self.google_cloud_id
        result = self.user_auth.get_google_cloud_id(self.username)
        self.assertEqual(expected, result)

    def test_get_google_cloud_id_with_invalid_username(self):
        result = self.user_auth.get_google_cloud_id("fake@email.com")
        self.assertEqual(None, result)

    def test_change_google_cloud_id_with_valid_account(self):
        result = self.user_auth.change_google_cloud_id(self.username, "new")
        new_key = self.user_auth.get_google_cloud_id(self.username)
        self.assertTrue(result)
        self.assertEqual("new", new_key)

    def test_change_google_cloud_id_with_invalid_account(self):
        result = self.user_auth.change_google_cloud_id("fake", "new")
        self.assertEqual(False, result)

    """ get_sms_database() """
    def test_get_sms_database(self):
        result = self.user_auth.get_sms_database(self.username)
        self.assertEqual(my_setup.sms_file, result)

    def test_get_sms_database_return_false_if_not_found(self):
        result = self.user_auth.get_sms_database("nonexistent@email.com")
        self.assertEqual(False, result)

    """ delete_account() """
    def test_delete_account_with_valid_username(self):
        result = self.user_auth.delete_account(self.username)
        conn = sqlite3.connect(my_setup.user_file)
        cur = conn.cursor()
        statement = "SELECT * FROM users WHERE username = ?"
        cur.execute(statement, (self.username,))
        self.assertTrue(result)
        self.assertEqual(None, cur.fetchone())

    def test_delete_account_invalid_username_returns_false(self):
        result = self.user_auth.delete_account("nope@email.com")
        self.assertEqual(False, result)
