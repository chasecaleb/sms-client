import unittest
from tests import my_setup
import sms_database


class TestMessages(unittest.TestCase):
    """ Contacts is nearly identical to Messages, so it isn't tested. """

    def setUp(self):
        # Provides temp files, file location strings, and temp file cleanup
        my_setup.setup_files(self)
        self.messages = sms_database.Messages(my_setup.sms_file)

    """ __getitem__() -- single element """
    def test_getitem_field_names(self):
        expected_fields = (
            "msg_id", "thread_id", "address", "date", "msg_type", "body")
        self.assertTupleEqual(expected_fields, self.messages[0]._fields)

    def test_getitem(self):
        for i, sms in enumerate(my_setup.messages):
            self.assertTupleEqual(sms, tuple(self.messages[i]))

    def test_getitem_negative_index_returns_from_end(self):
        self.assertTupleEqual(my_setup.messages[-1], self.messages[-1])

    def test_getitem_key_above_max_raises_error(self):
        self.assertRaises(IndexError, lambda: self.messages[999])

    def test_getitem_invalid_key_type_raises_error(self):
        self.assertRaises(TypeError, lambda: self.messages["a"])

    """ __getitem__() -- slicing """
    def test_getitem_string_slice_key_raises_error(self):
        self.assertRaises(TypeError, lambda: self.messages["a":])
        self.assertRaises(TypeError, lambda: self.messages[:"b"])
        self.assertRaises(TypeError, lambda: self.messages["a":"b"])

    def test_getitem_with_step_slice_ignores_step(self):
        self.assertListEqual(list(self.messages), self.messages[::2])

    def test_getitem_using_start_slice_returns_last_two(self):
        start = len(my_setup.messages) - 2
        self.assertListEqual(my_setup.messages[start:], self.messages[start:])

    def test_getitem_using_stop_slice_returns_first_two(self):
        self.assertListEqual(my_setup.messages[:2], self.messages[:2])

    def test_getitem_using_start_and_stop_slice_returns_middle(self):
        self.assertListEqual(my_setup.messages[2:4], self.messages[2:4])

    def test_getitem_using_negative_start_slice_returns_last(self):
        self.assertListEqual(my_setup.messages[-1:], self.messages[-1:])

    def test_getitem_using_negative_stop_slice_return_only_first(self):
        self.assertListEqual(my_setup.messages[:-5], self.messages[:-5])

    def test_getitem_using_negative_start_stop_slice_returns_middle(self):
        self.assertListEqual(my_setup.messages[-4:-2], self.messages[-4:-2])

    def test_getitem_using_out_of_bounds_stop_slice_returns_to_end(self):
        self.assertListEqual(self.messages[:1000], list(self.messages))
        self.assertListEqual(my_setup.messages, self.messages[:1000])

    def test_getitem_using_overlapped_slice_returns_empty_list(self):
        """ Also catches case where [_:0] becomes [_:len(messages)]. """
        self.assertListEqual([], self.messages[1:0])

    """ append() """
    def test_append_with_list(self):
        new_msg = [99, 3, "8885550000", 1001, 2, "new msg"]
        self.messages.append(new_msg)
        self.assertListEqual(new_msg, list(self.messages[-1]))
        query = self.messages._execute(
            "SELECT * FROM sms WHERE msg_id=99")
        self.assertListEqual(new_msg, list(query.fetchone()))

    def test_append_with_dict(self):
        new_msg = {"msg_id": 99, "thread_id": 3, "address": "8885550000",
                   "date": 1499999999000, "msg_type": 2, "body": 
                   "new sent msg"}
        self.messages.append(new_msg)
        self.assertDictEqual(new_msg, self.messages[-1]._asdict())
        query = self.messages._execute(
            "SELECT * FROM sms WHERE msg_id=99")
        self.assertDictEqual(new_msg, query.fetchone()._asdict())

    def test_append_overwrites_existing(self):
        new_msg = (6,) + self.messages[1][1:]
        self.assertNotEqual(new_msg, self.messages[5])
        self.messages.append(new_msg)
        self.assertTupleEqual(new_msg, self.messages[5])
        query = self.messages._execute("SELECT * FROM sms WHERE msg_id = 6")
        self.assertListEqual(list(new_msg), list(query.fetchone()))

    """ remove()/__delitem__() """
    def test_delitem_deletes_existing_element(self):
        self.assertIn(my_setup.messages[3], self.messages)
        del self.messages[3]
        self.assertNotIn(my_setup.messages[3], self.messages)

    def test_delitem_raises_index_error_on_nonexistent_element(self):
        # Actual syntax is "del self.messages[999]", but that's not testable.
        self.assertRaises(IndexError, lambda: self.messages.__delitem__(999))

    def test_remove_existing_element(self):
        msg_id = my_setup.messages[3][0]
        self.assertIn(my_setup.messages[3], self.messages)
        self.messages.remove(msg_id)
        self.assertNotIn(my_setup.messages[3], self.messages)

    def test_remove_raises_value_error_on_nonexistent_element(self):
        self.assertRaises(ValueError, lambda: self.messages.remove(999))

    """ find() """
    def test_find_existing_element_returns_index(self):
        msg_id = my_setup.messages[3][0]
        self.assertEqual(3, self.messages.find(msg_id))

    def test_find_raises_value_error_on_nonexistent_element(self):
        self.assertRaises(ValueError, lambda: self.messages.find(999))

    """ misc """
    def test_len(self):
        self.assertEqual(7, len(self.messages))

    def test_repr(self):
        self.assertEqual(repr(self.messages), repr(list(self.messages)))

    def test_iter(self):
        self.assertListEqual(my_setup.messages, list(self.messages))
        self.assertListEqual(my_setup.messages, list(iter(self.messages)))

    def test_contains_existing_row_returns_true(self):
        self.assertTrue(self.messages[1] in self.messages)
        self.assertTrue(my_setup.messages[1] in self.messages)

    def test_contains_nonexistent_row_returns_false(self):
        new_msg = (3, "8885550000", 140000000, 2, "new message")
        result = new_msg in self.messages
        self.assertFalse(result)
        self.assertIsNotNone(result)

    """ threads """
    def test_threads_length(self):
        self.assertEqual(2, len(self.messages.threads))

    def test_threads(self):
        thread_a = {'name': '8885551234', 'message': my_setup.messages[2]}
        thread_b = {'name': 'Alter Ego', 'message': my_setup.messages[6]}
        expected = [thread_a, thread_b]
        self.assertListEqual(expected, self.messages.threads)
    # def test_threads_iter(self):
        # expected = [my_setup.messages[2], my_setup.messages[6]]
        # self.assertListEqual(expected, self.messages.threads)

    # def test_get_thread_messages(self):
        # expected = [my_setup.messages[i] for i in (0, 1, 2)]
        # self.assertListEqual(expected, self.messages.get_thread_messages(6))

    # """ threads_by_name """
    # def test_threads_by_name(self):
        # expected = ['8885551234', 'Alter Ego']
        # self.assertEqual(expected, self.messages.threads_by_name)


class TestContacts(unittest.TestCase):

    def setUp(self):
        my_setup.setup_files(self)
        self.contacts = sms_database.Contacts(my_setup.sms_file)

    def test_preceding_plus_one_address_lookup(self):
        """ Android (or at least my phone) likes to use this format. """
        result = self.contacts.lookup_address("+18885554567")
        self.assertEqual(my_setup.contacts[0], result)

    def test_unformatted_address_lookup(self):
        result = self.contacts.lookup_address("8885554567")
        self.assertEqual(my_setup.contacts[0], result)

    def test_hyphenated_address_lookup(self):
        result = self.contacts.lookup_address("888-555-4567")
        self.assertEqual(my_setup.contacts[0], result)

    def test_heavily_formatted_address_lookup(self):
        result = self.contacts.lookup_address("+1(888) 555-4567")
        self.assertEqual(my_setup.contacts[0], result)

    def test_lookup_address_returns_False_if_not_found(self):
        result = self.contacts.lookup_address("1234567890")
        self.assertEqual(False, result)

if __name__ == "__main__":
    unittest.main()
