#!/bin/bash

function run_and_report ()
{
    echo; echo; echo; echo; echo # Whitespace
    time=`date +%I:%M:%S`
    echo "($time) -- RUNNING TESTS"
    coverage run --omit='tests/*' -m unittest discover 'tests/'
}

# Run test right away, then repeat when files are modified
run_and_report
coverage report -m
echo; echo "Quit and re-run to see coverage report again."; echo
inotifywait --monitor --recursive --format %f --exclude=".db|.pyc|.coverage" \
        --event close_write "./" | while read info; do
    regex="\.py+$"
    if [[ $info =~ $regex ]]; then
        run_and_report
    fi
done
