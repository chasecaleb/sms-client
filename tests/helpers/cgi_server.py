#!/usr/bin/env python
import http.server
import cgitb

""" CGI server to test android_upload.py locally. Run from project root dir."""


def cgi_server(cgi_directory):
    server = http.server.HTTPServer
    handler = http.server.CGIHTTPRequestHandler
    server_address = ("", 8000)
    handler.cgi_directories = [cgi_directory]
    return server(server_address, handler)

if __name__ == "__main__":
    cgitb.enable()
    print("CGI server running.")
    httpd = cgi_server("/")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print()
        print("Quitting.")
        raise SystemExit
