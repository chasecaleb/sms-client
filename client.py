#!/usr/bin/env python
import queue
import urwid
from client_backend import ClientBackend


class MainView(urwid.Columns):

    def __init__(self, event_loop):
        self.event_loop = event_loop
        self.queue_in = event_loop.watch_queue(self.handle_queue)
        self.queue_out = queue.Queue()

        self.backend = ClientBackend(self.queue_out, self.queue_in)
        self.backend.start()

        self.queue_out.put([None, "messages.threads"])
        threads = self.queue_in.get()[1]
        self.conversation = ConversationFrame(self.event_loop, self.queue_out,
                                              threads[-1])
        self.menu = ThreadView(self.event_loop, self.queue_out)
        super().__init__(((30, self.menu), self.conversation))

    def keypress(self, size, key):
        # Thread list (sidebar)
        if self.focus_position == 0:
            if key == "enter":
                thread_focus = self.focus.original_widget.focus_position
                thread_info = self.focus.threads[thread_focus]
                conversation = ConversationFrame(
                    self.event_loop, self.queue_out, thread_info)
                self.contents[1] = conversation, self.contents[1][1]
                self.focus_position = 1
        return super(MainView, self).keypress(size, key)

    def handle_queue(self, contents):
        pass


# TODO: move this, ListButton, and (?) EventLoop to separate file
class VerticalSeparator(urwid.WidgetDecoration, urwid.WidgetWrap):
    """ Helper class to wrap widget in a vertical separator. """

    def __init__(self, original_widget, left_char="│", right_char="│"):
        """Draw a separator on left and/or right of original_widget."""
        widgets = [original_widget]
        if left_char:
            widgets.insert(0, ('fixed', 1, urwid.SolidFill(left_char)))
        if right_char:
            widgets.append(('fixed', 1, urwid.SolidFill(right_char)))
        columns = urwid.Columns(widgets, box_columns=[0, 2], focus_column=1)
        urwid.WidgetDecoration.__init__(self, original_widget)
        urwid.WidgetWrap.__init__(self, columns)


class ListButton(urwid.Button):
    """ Helper class for menus. """

    # TODO: fix 2nd character being highlighted
    def __init__(self, text, callback=None):
        if callback is not None:
            super(ListButton, self).__init__(text, on_press=callback)
        else:
            super(ListButton, self).__init__(text)
        self._w = urwid.AttrMap(urwid.SelectableIcon(text), None,
                                focus_map="reversed")


class ThreadView(VerticalSeparator):

    def __init__(self, event_loop, queue_out):
        placeholder = [ListButton("Loading...")]
        self.thread_walker = urwid.SimpleListWalker(placeholder)
        super().__init__(urwid.ListBox(self.thread_walker), left_char=' ')

        self.event_loop = event_loop
        self.queue_out = queue_out
        self.queue_out.put([self.update, "messages.threads"])

    def update(self, threads):
        self.threads = threads
        list_buttons = [ListButton(i['name']) for i in threads]
        thread_walker = urwid.SimpleListWalker([
            urwid.AttrMap(i, None, "reveal focus") for i in list_buttons])
        self.thread_walker[:] = thread_walker


class ConversationFrame(urwid.Frame):

    def __init__(self, event_loop, queue_out, thread_info):
        self.event_loop = event_loop
        self.queue_out = queue_out
        self.name = thread_info['name']
        header_text = (" Conversation with " + thread_info['name'] + " (" + 
                       thread_info['message'].address + ")")
        self.body = self.get_body([])
        self.header = urwid.Text(header_text)
        # TODO: replace footer with something that actually sends
        self.footer = urwid.Edit(" Reply: ")
        super(ConversationFrame, self).__init__(
            self.body, self.header, self.footer, focus_part="footer")

        self.queue_out.put([self.update, "messages.get_thread_messages",
                            thread_info['message'].thread_id])

    def update(self, messages):
        self.body = self.get_body(messages)

    def get_body(self, messages):
        message_list = []
        layout = '{0:>{1}}: {2}'
        for msg in messages:
            # msg["msg_type"] == direction (1 for incoming, 2 for outgoing)
            sender = self.name if msg.msg_type == 1 else "Me"
            text = layout.format(sender, len(self.name), msg.body)
            message_list.append(ListButton(text))
        message_listbox = urwid.ListBox(urwid.SimpleListWalker(message_list))
        # Puts a horizontal divider along the top and bottom
        return urwid.LineBox(
            message_listbox, tlcorner=" ", lline=" ", trcorner=" ",
            blcorner=" ", rline=" ", brcorner=" ")


class EventLoop(urwid.SelectEventLoop):
    """ Custom event loop with ability to watch queue.Queue()'s.

    Setup Queue watches via watch_queue(), remove watches with
    remove_watch_queue(). Works like default watch_pipe()/remove_watch_pipe()
    methods.
    """

    def __init__(self):
        super().__init__()
        self._watch_queues = {}
        self.enter_idle(self._poll_queues)

    def watch_queue(self, default_callback):
        q = queue.Queue()
        self._watch_queues[q] = default_callback
        return q

    def remove_watch_queue(self, q):
        try:
            del self._watch_queues[q]
            return True
        except KeyError:
            return False

    def _poll_queues(self):
        for q in self._watch_queues:
            try:
                func, data = q.get_nowait()
                # Fallback to default callback if one wasn't received
                callback = func if func else self._watch_queues[q]
                try:
                    result = callback(data)
                except TypeError:
                    result = callback()
                if result is False:
                    self.remove_watch_queue(q)
            except queue.Empty:
                pass

            self.alarm(0.1, self._poll_queues)

if __name__ == "__main__":
    palette = [("reversed", "standout", "")]
    event_loop = EventLoop()
    loop = urwid.MainLoop(MainView(event_loop), palette, event_loop=event_loop)
    try:
        loop.run()
    except KeyboardInterrupt:
        print("Exiting")
        raise SystemExit
